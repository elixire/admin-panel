import { Intent, Position, Toaster } from '@blueprintjs/core'

const AppToaster = Toaster.create({
  className: 'eap-toaster',
  position: Position.BOTTOM_RIGHT,
})

export default AppToaster

export function success(message, icon = 'tick') {
  AppToaster.show({
    icon,
    intent: Intent.SUCCESS,
    message,
  })
}

export function warning(message, icon = 'warning-sign') {
  AppToaster.show({
    icon,
    intent: Intent.WARNING,
    message,
  })
}

export function failure(message, icon = 'error') {
  AppToaster.show({
    icon,
    intent: Intent.DANGER,
    message,
  })
}
