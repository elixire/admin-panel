import {
  Button,
  ControlGroup,
  InputGroup,
  Intent,
  Popover,
  Position,
} from '@blueprintjs/core'
import debounce from 'lodash.debounce'
import React from 'react'
import { withRouter } from 'react-router-dom'

import Buttons from '../components/Buttons'
import Domain from '../api/domain'
import FileItem from '../api/file'
import File from '../components/File'
import FileDeleteAlert from '../components/FileDeleteAlert'
import FileMoveDialog from '../components/FileMoveDialog'
import ItemSearcher, { ItemType } from '../components/ItemSearcher'
import LoadingText from '../components/LoadingText'
import PopoverContent from '../components/PopoverContent'
import Shorten from '../components/Shorten'
import ShortenItem from '../api/shorten'
import { failure, success } from '../toaster'

class Files extends React.Component {
  state = {
    domains: null,
    fileDeleteOpen: false,
    fileMoveOpen: false,
    item: null,
    loading: false,
    loadingDomains: false,
    query: this.props.match.params.id || '',
    renameLoading: false,
    renamingTo: '',
    shorten: this.props.location.search === '?shorten=true' || false,
  }

  async componentDidMount() {
    this.setState({ loadingDomains: true })

    try {
      this.setState({ domains: await Domain.fetchAll() })
    } catch (err) {
      failure('Failed to fetch domains.')
    } finally {
      this.setState({ loadingDomains: false })
    }

    if (this.state.query != null) {
      this.load()
    }
  }

  fetchDebounced = debounce(() => this.load(), 500)

  getUrl() {
    if (this.state.domains == null || this.state.item == null) {
      return '...'
    }

    const domain = this.state.item.domain.validDomain
    return this.state.item.getUrl(domain)
  }

  async load() {
    this.setState({ loading: true })
    await this.fetch()
    this.setState({ loading: false })
  }

  async fetch() {
    const { query } = this.state

    const bail = () => {
      this.setState({ item: null })
      this.props.history.replace('/files')
    }

    if (query === '') {
      bail()
      return
    }

    const item = this.state.shorten
      ? await ShortenItem.getByShortcode(this.state.query)
      : await FileItem.getByShortcode(this.state.query)

    if (item == null) {
      bail()
      return
    }

    // Fetch extended information.
    await item.fetchDomain()
    await item.fetchUploader()

    this.replaceRoute()
    this.setState({ item })
  }

  replaceRoute() {
    let route = `/files/${this.state.query}`

    if (this.state.shorten) {
      route += '?shorten=true'
    }

    this.props.history.replace(route)
  }

  renderButtons() {
    if (this.state.item == null) {
      return null
    }

    return (
      <Buttons>
        <Button
          icon="trash"
          intent={Intent.DANGER}
          onClick={this.handleOpenDeleteAlert}
          disabled={this.state.item.deleted}
        >
          Delete
        </Button>
        <Popover position={Position.BOTTOM}>
          <Button icon="edit">Rename</Button>
          <PopoverContent>
            <ControlGroup>
              <InputGroup
                placeholder="Shortname..."
                disabled={this.state.renameLoading}
                value={this.state.renamingTo}
                onChange={this.handleRenameToChange}
              />
              <Button
                intent={Intent.SUCCESS}
                loading={this.state.renameLoading}
                disabled={this.state.renamingTo === ''}
                onClick={this.handleRenameClick}
                icon="edit"
              >
                Rename
              </Button>
            </ControlGroup>
          </PopoverContent>
        </Popover>
        <Button icon="move" onClick={this.handleOpenFileMoveDialog}>
          Move
        </Button>
      </Buttons>
    )
  }

  renderDialogs() {
    return (
      <>
        <FileDeleteAlert
          isOpen={this.state.fileDeleteOpen}
          onCancel={this.handleFileDeleteAlertCancel}
          onConfirm={this.handleDeleteItem}
          file={this.getUrl()}
        />
        {this.state.item ? (
          <FileMoveDialog
            isOpen={this.state.fileMoveOpen}
            file={this.state.item}
            onClose={this.handleFileMoveDialogClose}
            onMoved={this.handleItemMoved}
          />
        ) : null}
      </>
    )
  }

  render() {
    let item = null

    if (this.state.item != null) {
      if (this.state.shorten) {
        item = <Shorten url={this.getUrl()} shorten={this.state.item} />
      } else {
        item = <File url={this.getUrl()} file={this.state.item} />
      }
    }

    let content = null

    if (this.state.loadingDomains) {
      content = <LoadingText>Loading domains...</LoadingText>
    } else if (this.state.loading) {
      content = <LoadingText>Loading...</LoadingText>
    } else if (item == null) {
      content = <div style={{ margin: '1rem 0' }}>Nothing found.</div>
    } else {
      content = (
        <>
          {item}
          {this.renderButtons()}
          {this.renderDialogs()}
        </>
      )
    }

    return (
      <>
        <ItemSearcher
          currentType={this.state.shorten ? ItemType.SHORTEN : ItemType.FILE}
          disabled={this.state.loadingDomains}
          onItemTypeChange={this.handleItemTypeChange}
          onChange={this.handleQueryChange}
          value={this.state.query}
        />

        {content}
      </>
    )
  }

  handleFileDeleteAlertCancel = () => {
    this.setState({ fileDeleteOpen: false })
  }

  handleFileMoveDialogClose = () => {
    this.setState({ fileMoveOpen: false })
  }

  handleQueryChange = (query) => {
    this.setState({ query }, this.fetchDebounced)
  }

  handleItemTypeChange = (type) => {
    this.setState(
      {
        item: null,
        shorten: type === ItemType.SHORTEN,
      },
      this.load
    )
  }

  handleDeleteItem = async () => {
    this.setState({ fileDeleteOpen: false })

    try {
      await this.state.item.delete()
      success(`Deleted ${this.state.item}.`, 'trash')
      this.load()
    } catch (err) {
      failure(`Failed to delete ${this.state.item}: ${err}`)
    }
  }

  handleOpenDeleteAlert = () => {
    this.setState({ fileDeleteOpen: true })
  }

  handleOpenFileMoveDialog = () => {
    this.setState({ fileMoveOpen: true })
  }

  handleItemMoved = () => {
    this.setState({ fileMoveOpen: false })
    this.load()
  }

  handleRenameToChange = (event) => {
    this.setState({ renamingTo: event.target.value })
  }

  handleRenameClick = async () => {
    this.setState({ renameLoading: true })

    try {
      await this.state.item.rename(this.state.renamingTo)
      success(
        `Renamed ${this.state.item.shortcode} to ${this.state.renamingTo}.`,
        'edit'
      )
      this.setState(
        (prevState) => ({
          query: prevState.renamingTo,
        }),
        this.load
      )
    } catch (err) {
      failure(`Failed to rename ${this.state.item.shortcode}: ${err}`)
    } finally {
      this.setState({ renameLoading: false })
    }
  }
}

export default withRouter(Files)
