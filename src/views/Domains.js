import { Button, InputGroup, Intent } from '@blueprintjs/core'
import React from 'react'
import 'styled-components/macro'
import debounce from 'lodash.debounce'

import Domain from '../api/domain'
import DomainAddDialog from '../components/DomainAddDialog'
import DomainEditDialog from '../components/DomainEditDialog'
import DomainDeleteAlert from '../components/DomainDeleteAlert'
import DomainListing from '../components/DomainListing'
import LoadingText from '../components/LoadingText'
import PaginatorControl from '../components/PaginatorControl'
import { failure, success } from '../toaster'
import PerPageSlider from '../components/PerPageSlider'

export default class Domains extends React.Component {
  state = {
    deletingDomain: null,
    domainAddOpen: false,
    domainDeleteOpen: false,
    domainEditOpen: false,
    domains: [],
    editingDomain: null,
    firstDomain: null,
    loading: false,
    page: 0,
    pagination: { total: 0 },
    perPage: 10,
    query: '',
  }

  componentDidMount() {
    this.load({ loadFirst: true })
  }

  loadFirstDomain = async () => {
    try {
      const { results } = await Domain.search({ page: 0, per_page: 1 })
      this.setState({ firstDomain: results[0] })
    } catch (err) {
      console.error(err)
      failure(`Failed to load first domain: ${err}`)
    }
  }

  loadDomains = async () => {
    try {
      const { pagination, results } = await Domain.search({
        page: this.state.page,
        per_page: this.state.perPage,
        query: this.state.query,
      })

      this.setState({ domains: results, pagination })
    } catch (err) {
      console.error(err)
      failure(`Failed to load domain list: ${err}`)
    }
  }

  load = async ({ loadFirst = false } = {}) => {
    this.setState({ loading: true })
    await this.loadDomains()
    if (loadFirst) {
      await this.loadFirstDomain()
    }
    this.setState({ loading: false })
  }

  loadDebounced = debounce(() => this.load(), 500)

  renderDomainListing() {
    const { loading, domains } = this.state

    if (loading) {
      return <LoadingText>Loading domains...</LoadingText>
    }

    return (
      <DomainListing
        domains={domains}
        onDelete={this.handleDomainDelete}
        onEdit={this.handleDomainEdit}
      />
    )
  }

  renderDialogs() {
    const { firstDomain } = this.state

    return (
      <>
        <DomainAddDialog
          isOpen={this.state.domainAddOpen}
          onClose={this.handleDomainAddDialogClose}
          onAdded={this.handleDomainAdd}
          onSubmit={this.handleDomainAddSubmit}
        />
        <DomainEditDialog
          isOpen={this.state.domainEditOpen}
          onClose={this.handleDomainEditDialogClose}
          domain={this.state.editingDomain}
          onSubmit={this.handleDomainEditSubmit}
        />
        {this.state.deletingDomain != null ? (
          <DomainDeleteAlert
            isOpen={this.state.domainDeleteOpen}
            domain={this.state.deletingDomain}
            movingTo={firstDomain}
            onCancel={this.handleDomainDeleteAlertCancel}
            onConfirm={this.handleDomainDeleteConfirm}
          />
        ) : null}
      </>
    )
  }

  render() {
    return (
      <>
        {this.renderDialogs()}

        <InputGroup
          leftIcon="search"
          placeholder="Search..."
          type="search"
          large
          onChange={this.handleQueryChange}
          value={this.state.query}
          css="margin-bottom: 1rem"
        />
        <Button
          intent={Intent.PRIMARY}
          icon="plus"
          onClick={this.handleAddDomainClick}
        >
          Add
        </Button>

        {this.renderDomainListing()}

        <PaginatorControl
          page={this.state.page}
          pagination={this.state.pagination}
          onPageChanged={this.handlePageChange}
        />

        <PerPageSlider
          value={this.state.perPage}
          onChange={this.handlePerPageChange}
          onRelease={this.load}
        />
      </>
    )
  }

  handlePerPageChange = (perPage) => {
    this.setState({ page: 0, perPage })
  }

  handlePageChange = (page) => {
    this.setState({ page }, this.load)
  }

  handleAddDomainClick = () => {
    this.setState({ domainAddOpen: true })
  }

  handleDomainAdd = () => {
    this.setState({ domainAddOpen: false })
    this.load()
  }

  handleDomainDelete = (domain) => {
    this.setState({ deletingDomain: domain, domainDeleteOpen: true })
  }

  handleDomainEdit = (domain) => {
    this.setState({
      domainEditOpen: true,
      editingDomain: domain,
    })
  }

  handleDomainDeleteConfirm = async () => {
    this.setState({ domainDeleteOpen: false })

    try {
      await this.state.deletingDomain.delete()
      success(`Deleted ${this.state.deletingDomain.domain}.`, 'trash')
    } catch (err) {
      failure(`Failed to delete domain: ${err}`)
    }

    this.load()
  }

  handleDomainAddSubmit = async (values, { setSubmitting }) => {
    try {
      await Domain.create(values)
      success(`Created ${values.domain}.`, 'plus')
      this.setState({ domainAddOpen: false })
      this.load()
    } catch (err) {
      failure(`Failed to add ${values.domain}: ${err}`)
    } finally {
      setSubmitting(false)
    }
  }

  handleDomainEditSubmit = async (values, { setSubmitting }) => {
    const { editingDomain: domain } = this.state

    try {
      await domain.edit(values)
      success(`Edited ${domain.domain}.`, 'edit')
      this.setState({ domainEditOpen: false })
      this.load()
    } catch (err) {
      failure(`Failed to edit ${domain.domain}: ${err}`)
    } finally {
      setSubmitting(false)
    }
  }

  handleDomainAddDialogClose = () => {
    this.setState({ domainAddOpen: false })
  }

  handleDomainEditDialogClose = () => {
    this.setState({ domainEditOpen: false })
  }

  handleDomainDeleteAlertCancel = () => {
    this.setState({ domainDeleteOpen: false })
  }

  handleQueryChange = (event) => {
    this.setState({ query: event.currentTarget.value })
    this.loadDebounced()
  }
}
