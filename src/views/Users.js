import { InputGroup, Tab, Tabs } from '@blueprintjs/core'
import debounce from 'lodash.debounce'
import React from 'react'
import 'styled-components/macro'

import User from '../api/user'
import PaginatorControl from '../components/PaginatorControl'
import LoadingText from '../components/LoadingText'
import UserListing from '../components/UserListing'
import { failure, success } from '../toaster'
import PerPageSlider from '../components/PerPageSlider'

export default class Users extends React.Component {
  state = {
    loading: false,
    page: 0,
    pagination: { total: 0 },
    perPage: 10,
    query: '',
    selectedFilter: 'user-active-tab',
    users: [],
  }

  componentDidMount() {
    this.searchForUsers()
  }

  searchForUsersDebounced = debounce(() => {
    return this.searchForUsers()
  }, 500)

  searchForUsers = async () => {
    this.setState({ loading: true })
    await this.search()
    this.setState({ loading: false })
  }

  async search() {
    const users = await User.search({
      active: this.state.selectedFilter === 'user-active-tab',
      page: this.state.page,
      per_page: this.state.perPage,
      query: this.state.query,
    })

    this.setState({
      pagination: users.pagination,
      users: users.results,
    })
  }

  renderListing() {
    if (this.state.loading) {
      return <LoadingText>Loading users...</LoadingText>
    }

    if (this.state.users.length === 0) {
      return 'Nothing here.'
    }

    return this.state.users.map((user) => (
      <UserListing
        key={user.id}
        user={user}
        onDelete={() => this.handleDeleteUser(user)}
        onSendEmail={() => this.handleSendEmail(user)}
        onEnable={() => this.handleChangeActivationState(user, true)}
        onDisable={() => this.handleChangeActivationState(user, false)}
      />
    ))
  }

  render() {
    return (
      <>
        <InputGroup
          leftIcon="search"
          placeholder="Search by username or ID..."
          type="search"
          large={true}
          onChange={this.handleQueryChange}
          value={this.state.query}
        />

        <Tabs
          css="margin: 1rem 0"
          id="user-filter-tabs"
          onChange={this.handleFilterChange}
          selectedTabId={this.state.selectedFilter}
        >
          <Tab title="Active" id="user-active-tab" />
          <Tab title="Inactive" id="user-inactive-tab" />
        </Tabs>

        <div css="margin-top: 1rem">{this.renderListing()}</div>

        <PaginatorControl
          page={this.state.page}
          pagination={this.state.pagination}
          onPageChanged={this.handlePageChanged}
        />

        <PerPageSlider
          value={this.state.perPage}
          onChange={this.handlePerPageChange}
          onRelease={this.searchForUsers}
        />
      </>
    )
  }

  handlePageChanged = (page) => {
    this.setState({ page }, this.searchForUsers)
  }

  handlePerPageChange = (value) => {
    this.setState({
      page: 0,
      perPage: value,
    })
  }

  handleQueryChange = (event) => {
    this.setState({ page: 0, query: event.target.value })
    this.searchForUsersDebounced()
  }

  handleFilterChange = (newTabId) => {
    this.setState({ page: 0, selectedFilter: newTabId }, this.searchForUsers)
  }

  handleSendEmail = async (user) => {
    try {
      await user.sendEmail()
      success(`Sent email to ${user.name}.`, 'envelope')
    } catch (err) {
      failure(`Failed to send email: ${err}`)
    }
  }

  handleChangeActivationState = async (user, newState: boolean) => {
    await user.changeActivationState(newState)
    this.searchForUsers()
  }

  handleDeleteUser = async (user) => {
    try {
      await user.delete()
      success(`Deleted ${user.name}.`, 'trash')
    } catch (err) {
      failure(`Failed to delete ${user.name}: ${err}`)
    }
    this.searchForUsers()
  }
}
