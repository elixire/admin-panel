import React, { useState, useEffect } from 'react'

import LoadingText from '../components/LoadingText'
import Settings from '../api/settings'
import AdminSetting from '../components/AdminSetting'
import { failure } from '../toaster'

const settingLang = {
  audit_log_emails: {
    name: 'Send me audit log emails',
    description:
      'Receive emails detailing important stuff that your fellow admins have done.',
  },
}

export default function SettingsPage() {
  const [settings, setSettings] = useState(null)

  async function fetchSettings() {
    try {
      setSettings(await Settings.get())
    } catch (err) {
      failure(`Failed to load settings: ${err}`)
    }
  }

  async function changeSetting(name, value) {
    await Settings.set({ [name]: value })
  }

  useEffect(() => {
    fetchSettings()
  }, [])

  if (settings == null) {
    return (
      <>
        <h2>Settings</h2>
        <LoadingText>Loading settings...</LoadingText>
      </>
    )
  }

  const settingEntries = Object.keys(settings).map((name) => {
    const { name: humanName = name, description = name } =
      settingLang[name] || {}

    async function handleChange({ target: { checked } }) {
      await changeSetting(name, checked)
      setSettings({ ...settings, [name]: checked })
    }

    return (
      <AdminSetting
        key={name}
        name={humanName}
        description={description}
        enabled={settings[name]}
        onChange={handleChange}
      />
    )
  })

  return (
    <>
      <h2>Settings</h2>
      {settingEntries}
    </>
  )
}
