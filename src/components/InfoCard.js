import { Card } from '@blueprintjs/core'
import styled from 'styled-components'

const InfoCard = styled(Card)`
  margin: 1rem 0;

  p {
    margin-bottom: 0;
  }
`

export default InfoCard
