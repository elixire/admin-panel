import styled from 'styled-components'

export const Page = styled.div`
  padding: 2rem;
`

export const PageWrapper = styled.div`
  display: grid;
  grid-template-columns: minmax(250px, 1fr) 4fr;
`
