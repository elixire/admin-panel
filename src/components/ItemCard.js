/* eslint-disable jsx-a11y/anchor-is-valid */

import React from 'react'
import styled, { css } from 'styled-components'

import { toDate } from '../snowflake'
import CardHeader from './CardHeader'
import InfoCard from './InfoCard'

const StrikeableCardHeader = styled(({ strike, ...rest }) => (
  <CardHeader {...rest} />
))`
  ${(props) =>
    props.strike &&
    css`
      text-decoration: line-through;
    `};
`

const Timestamp = styled.time`
  display: block;
  margin-top: 0.5rem;
  font-size: 0.75rem;
  opacity: 0.85;
`

export default function ItemCard({ deleted, url, title, children, item }) {
  const created = toDate(item.id)

  return (
    <InfoCard>
      <a href={deleted ? undefined : url}>
        <StrikeableCardHeader strike={deleted}>{title}</StrikeableCardHeader>
      </a>
      <p>{children}</p>
      <Timestamp title={`${created.toISOString()} UTC`}>
        Created {created.toLocaleString()}
      </Timestamp>
    </InfoCard>
  )
}
