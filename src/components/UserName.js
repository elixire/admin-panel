import { Icon } from '@blueprintjs/core'
import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const StyledUserName = styled.div`
  display: flex;
  align-items: center;

  .bp3-icon {
    opacity: 0.5;
    margin-right: 0.25em;
    height: 1em;

    svg {
      height: inherit;
    }
  }
`

export default function UserName({ user, className }) {
  return (
    <StyledUserName className={className}>
      {user.admin ? <Icon icon="star" /> : null}
      <span>{user.name}</span>
    </StyledUserName>
  )
}

UserName.propTypes = {
  user: PropTypes.shape({
    admin: PropTypes.bool.isRequired,
    name: PropTypes.string.isRequired,
  }).isRequired,
}
