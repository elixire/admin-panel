import { Button, Dialog, Intent, MenuItem } from '@blueprintjs/core'
import { Select } from '@blueprintjs/select'
import React from 'react'

import Domain from '../api/domain'
import File from '../api/file'
import { failure, success } from '../toaster'
import { pluralize } from '../util'
import DialogContent from './DialogContent'
import LoadingText from './LoadingText'

export default class FileMoveDialog extends React.Component {
  state = {
    destination: null,
    domains: null,
    loading: false,
    moving: false,
    selectedDomain: null,
  }

  componentDidMount() {
    this.load()
  }

  async load() {
    this.setState({ loading: true })
    await this.fetch()
    this.setState({ loading: false })
  }

  async fetch() {
    const { results: domains } = await Domain.fetchAllAdmin()
    const firstId = Object.keys(domains)[0]
    const defaultDomain = domains[firstId]

    this.setState({
      domains,
      selectedDomain: defaultDomain,
    })
  }

  itemRenderer = (domain, { handleClick, modifiers }) => {
    const label = pluralize('file', domain.stats.files)

    return (
      <MenuItem
        active={modifiers.active}
        disabled={modifiers.disabled}
        key={domain.id}
        onClick={handleClick}
        label={label}
        text={domain.domain}
      />
    )
  }

  itemPredicate(query, { domain }) {
    return domain.toLowerCase().includes(query.toLowerCase())
  }

  itemDisabled = ({ domain }) => {
    return domain === this.props.file.domain.domain
  }

  render() {
    let selector = null

    if (this.state.loading || this.state.selectedDomain == null) {
      selector = <LoadingText>Loading domains...</LoadingText>
    } else if (this.state.domains != null) {
      selector = (
        <Select
          itemRenderer={this.itemRenderer}
          itemPredicate={this.itemPredicate}
          itemDisabled={this.itemDisabled}
          onItemSelect={this.handleItemSelect}
          items={this.state.domains}
          popoverProps={{ minimal: true, usePortal: false }}
        >
          <Button
            text={this.state.selectedDomain.domain}
            rightIcon="double-caret-vertical"
          />
        </Select>
      )
    }

    const type = this.props.file instanceof File ? 'file' : 'shorten'

    return (
      <Dialog title={`Move ${type}`} icon="move" {...this.props}>
        <DialogContent>
          <p>
            Move <strong>{this.props.file.shortcode}</strong> to:
          </p>

          {selector}

          <Button
            intent={Intent.SUCCESS}
            icon="tick"
            style={{ display: 'block', marginTop: '1rem' }}
            loading={this.state.moving}
            onClick={this.handleMoveClick}
          >
            Move
          </Button>
        </DialogContent>
      </Dialog>
    )
  }

  handleItemSelect = (selectedDomain) => {
    // Ignore if this domain is the one that the file is already on.
    const domain = this.props.file.domain
    if (selectedDomain.domain === domain.domain) {
      return
    }

    this.setState({ selectedDomain })
  }

  handleMoveClick = async () => {
    if (this.state.selectedDomain == null) {
      return
    }

    this.setState({ moving: true })

    try {
      await this.props.file.move(this.state.selectedDomain)
      success(
        `Moved ${this.props.file} to ${this.state.selectedDomain.domain}.`,
        'move'
      )
    } catch (err) {
      failure(`Failed to move ${this.props.file}: ${err}`)
    } finally {
      this.setState({ moving: false })
      this.props.onMoved()
    }
  }
}
