import { Dialog } from '@blueprintjs/core'
import React from 'react'
import { Formik } from 'formik'

import DomainAddForm from '../forms/DomainAddForm'
import DialogContent from './DialogContent'

export default function DomainAddDialog({ onSubmit, ...props }) {
  return (
    <Dialog title="Add domain" icon="plus" {...props}>
      <DialogContent>
        <Formik
          initialValues={{
            adminOnly: false,
            domain: '',
            official: false,
          }}
          component={DomainAddForm}
          onSubmit={onSubmit}
        />
      </DialogContent>
    </Dialog>
  )
}
