import React from 'react'
import { custom } from './DeleteAlert'

export default custom(({ file }) => (
  <p>
    Are you sure you want to delete <strong>{file}</strong>?
  </p>
))
