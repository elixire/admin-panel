import React from 'react'
import { Link } from 'react-router-dom'
import prettyBytes from 'pretty-bytes'

import ItemCard from './ItemCard'

export default function File({ file, url }) {
  const { uploader, size: fileSize, mimetype, deleted } = file
  const size = prettyBytes(fileSize)
  const how = deleted ? 'Originally uploaded' : 'Uploaded'

  if (typeof uploader === 'string') {
    throw TypeError('<File/> cannot render files with unexpanded uploaders')
  }

  return (
    <ItemCard url={url} title={url} deleted={deleted} item={file}>
      {how} by <Link to={`/user/${uploader.id}`}>{uploader.name}</Link> ({size},{' '}
      {mimetype})
    </ItemCard>
  )
}
