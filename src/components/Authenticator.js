import { Button, ControlGroup, InputGroup } from '@blueprintjs/core'
import React from 'react'

import { AuthenticationState, login } from '../api/auth'
import { failure, success } from '../toaster'

export default class Authenticator extends React.Component {
  state = {
    loading: false,
    token: window.localStorage.getItem('token') || '',
  }

  render() {
    return (
      <>
        <p>
          You can grab an admin token from your&nbsp;
          <a
            target="_blank"
            rel="noopener noreferrer"
            href="https://elixi.re/account.html"
          >
            account page.
          </a>
          &nbsp;Alternatively, just log in on the main website.
        </p>
        <ControlGroup vertical={false} fill={true}>
          <InputGroup
            className="bp3-monospace-text"
            leftIcon="lock"
            placeholder="elixi.re API key"
            disabled={this.state.loading}
            onChange={this.handleTokenChange}
            value={this.state.token}
          />
          <Button
            intent="success"
            text="Login"
            rightIcon="arrow-right"
            loading={this.state.loading}
            onClick={this.handleTokenSubmit}
          />
        </ControlGroup>
      </>
    )
  }

  handleTokenChange = ({ currentTarget: { value } }) => {
    this.setState({ token: value })
  }

  handleTokenSubmit = async () => {
    this.setState({ loading: true })

    const result = await login(this.state.token)
    const { state } = result

    if (state === AuthenticationState.INVALID) {
      const error = result.error
      const message =
        error.toString() === 'Error: HTTP 403'
          ? 'Invalid token.'
          : error.toString()

      failure(`Failed to login: ${message}`)
      console.error('[Login] Failed:', result.error)

      this.setState({ loading: false })
      return
    }

    // Save the token for later.
    window.localStorage.setItem('token', this.state.token)

    const user = result.user

    success(`Hello, ${user.name}!`, 'log-in')
    this.setState({ loading: false })

    if (this.props.onLogin) {
      this.props.onLogin(state)
    }
  }
}
