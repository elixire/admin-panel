import { Alignment, Button, Classes } from '@blueprintjs/core'
import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

const StyledNavLink = styled(Link)`
  display: block;
`

export default function NavLink(props) {
  return (
    <StyledNavLink to={props.to}>
      <Button
        className={Classes.MINIMAL}
        icon={props.icon}
        text={props.children}
        alignText={Alignment.LEFT}
        fill
        large
      />
    </StyledNavLink>
  )
}
