import styled from 'styled-components'

const CardHeader = styled.h5`
  display: flex;
  align-items: center;
  font-size: 1.3rem;
  margin: 0 0 0.5rem 0;
`

export default CardHeader
