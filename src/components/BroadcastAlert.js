import { Alert, Intent } from '@blueprintjs/core'
import React from 'react'

export default function BroadcastAlert(props) {
  return (
    <Alert
      cancelButtonText="Cancel"
      confirmButtonText="Broadcast"
      icon="headset"
      intent={Intent.SUCCESS}
      canEscapeKeyCancel
      canOutsideClickCancel
      {...props}
    >
      Broadcast this to all users?
    </Alert>
  )
}
