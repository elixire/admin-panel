import React from 'react'
import { custom } from './DeleteAlert'

export default custom(({ user }) => (
  <p>
    Are you sure you want to delete <strong>{user.name}</strong>? All of their
    files will be deleted.
  </p>
))
