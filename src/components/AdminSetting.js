import { Switch } from '@blueprintjs/core'
import React from 'react'
import styled from 'styled-components'

const StyledAdminSetting = styled.div`
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
`

const Information = styled.div`
  display: flex;
  flex-flow: column nowrap;
`

const Name = styled.div`
  margin-bottom: 0.5em;
`

const Description = styled.p`
  opacity: 0.5;
  margin: 0;
`

const SwitchContainer = styled.div`
  margin-left: auto;
  padding-left: 0.5rem;
`

export default class AdminSetting extends React.Component {
  render() {
    const { name, description, enabled, onChange } = this.props

    return (
      <StyledAdminSetting>
        <Information>
          <Name>{name}</Name>
          <Description>{description}</Description>
        </Information>
        <SwitchContainer>
          <Switch large checked={enabled} onChange={onChange} />
        </SwitchContainer>
      </StyledAdminSetting>
    )
  }
}
