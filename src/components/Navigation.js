import { Alignment, Button, Classes, Popover } from '@blueprintjs/core'
import React from 'react'
import styled from 'styled-components'

import { AuthenticationState } from '../api/auth'
import Authenticator from './Authenticator'
import NavLink from './NavLink'
import PopoverContent from './PopoverContent'

const StyledNavigation = styled.nav`
  padding: 2rem;
  width: 100%;
  min-height: 100vh;
  height: 100%;
  background: #ebf1f5;
  border-right: solid 1px #e1e8ed;
`

const Header = styled.h1`
  font-size: 1.5em;
  font-weight: inherit;
  margin: 0 0 0.5em;
`

const Group = styled.div`
  margin: 1rem 0;

  &:first-child {
    margin-top: 0;
  }
`

export default function Navigation({ authState, onTokenChanged }) {
  const adminLinks = (
    <>
      <NavLink icon="user" to="/users">
        Users
      </NavLink>
      <NavLink icon="document" to="/files">
        Files
      </NavLink>
      <NavLink icon="globe" to="/domains">
        Domains
      </NavLink>
      <NavLink icon="headset" to="/broadcast">
        Broadcast
      </NavLink>
      <NavLink icon="cog" to="/settings">
        Settings
      </NavLink>
    </>
  )

  return (
    <StyledNavigation>
      <Group>
        <Header>elixire admin</Header>
      </Group>
      <Group>
        {authState === AuthenticationState.ADMIN ? adminLinks : null}
        <NavLink icon="timeline-bar-chart" to="/domains/mine">
          My Domains
        </NavLink>
        <Popover targetTagName="div">
          <Button
            large
            className={Classes.MINIMAL}
            fill
            alignText={Alignment.LEFT}
            icon="log-in"
            text="Auth"
          />
          <PopoverContent>
            <Authenticator onLogin={onTokenChanged} />
          </PopoverContent>
        </Popover>
      </Group>
    </StyledNavigation>
  )
}
