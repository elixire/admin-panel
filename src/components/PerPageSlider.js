import React from 'react'
import { Slider } from '@blueprintjs/core'
import 'styled-components/macro'

export default function PerPageSlider(props) {
  return (
    <Slider
      css="max-width: 250px"
      stepSize={5}
      max={50}
      min={5}
      labelStepSize={5}
      {...props}
    />
  )
}
