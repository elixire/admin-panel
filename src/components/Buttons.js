import styled from 'styled-components'

const Buttons = styled.div`
  margin: 1rem 0;

  & *:not(:last-child) {
    margin-right: 0.5rem;
  }
`

export default Buttons
