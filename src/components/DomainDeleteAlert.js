import React from 'react'

import { custom } from './DeleteAlert'

export default custom(({ domain, movingTo }) => {
  const from = domain ? domain.domain : '...'
  const to = movingTo ? movingTo.domain : '...'

  return (
    <p>
      Are you sure you want to delete <strong>{from}</strong>? All users, files
      and shortens will be moved to <strong>{to}</strong>.
    </p>
  )
})
