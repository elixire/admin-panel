import React from 'react'
import styled from 'styled-components'
import prettyBytes from 'pretty-bytes'

import { pluralize } from '../util'

function summarizeSize(size) {
  if (size === 0) {
    // don't bother showing file size for no files
    return ''
  }

  return ` (${prettyBytes(size)})`
}

function formatStats(stats) {
  const { users, files, size, shortens } = stats
  const fileSummary = pluralize('file', files) + summarizeSize(size)

  return [
    pluralize('user', users),
    fileSummary,
    pluralize('shorten', shortens),
  ].join(', ')
}

const PublicStats = styled.span`
  color: #666;
`

export default function DomainStats({ stats, publicStats }) {
  const publicNode =
    publicStats != null ? (
      <PublicStats title="public statistics">
        ({formatStats(publicStats)})
      </PublicStats>
    ) : null

  return (
    <p>
      {formatStats(stats)} {publicNode}
    </p>
  )
}
