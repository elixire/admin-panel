import {
  Button,
  Intent,
  Menu,
  MenuItem,
  MenuDivider,
  Popover,
  Position,
} from '@blueprintjs/core'
import React from 'react'
import { Link } from 'react-router-dom'
import styled from 'styled-components'

import UserName from './UserName'

const StyledUserListing = styled.div`
  padding: 0.5rem;
  display: flex;
  flex-direction: row;
`

const Information = styled.div`
  display: flex;
  flex-direction: column;
`

const ID = styled.div`
  font-family: SFMono-Regular, Menlo, monospace;
  opacity: 0.5;
`

const Menus = styled.div`
  margin-left: auto;
`

const PaddedUserName = styled(UserName)`
  margin-bottom: 0.5em;
`

export default function UserListing({
  user,
  onDisable,
  onEnable,
  onSendEmail,
  onDelete,
}) {
  const { admin } = user

  return (
    <StyledUserListing>
      <Link to={`/user/${user.id}`}>
        <Information>
          <PaddedUserName user={user} />
          <ID>{user.id}</ID>
        </Information>
      </Link>
      <Menus>
        <Popover position={Position.BOTTOM_RIGHT} minimal>
          <Button icon="menu" />
          <Menu>
            {user.admin ? (
              <MenuItem icon="star" text="User is an admin" disabled />
            ) : null}
            <MenuDivider title="Quick Manage" />
            {user.active ? null : (
              <MenuItem
                icon="envelope"
                text="Send email"
                onClick={onSendEmail}
              />
            )}
            <MenuItem
              icon={user.active ? 'disable' : 'tick'}
              intent={user.active ? Intent.DANGER : Intent.SUCCESS}
              text={user.active ? 'Deactivate' : 'Activate'}
              onClick={user.active ? onDisable : onEnable}
              disabled={admin}
            />
            <MenuItem
              icon="trash"
              intent={Intent.DANGER}
              text="Delete"
              onClick={onDelete}
              disabled={admin}
            />
          </Menu>
        </Popover>
      </Menus>
    </StyledUserListing>
  )
}
