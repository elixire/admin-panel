import { Dialog } from '@blueprintjs/core'
import React from 'react'
import { Formik } from 'formik'

import DialogContent from './DialogContent'
import DomainEditForm from '../forms/DomainEditForm'

export default function DomainEditDialog({ domain, onSubmit, ...props }) {
  if (domain == null) {
    return null
  }

  return (
    <Dialog title={`Edit ${domain.domain}`} icon="edit" {...props}>
      <DialogContent>
        <Formik
          initialValues={{
            adminOnly: domain.adminOnly,
            official: domain.official,
            owner: domain.owner || undefined,
          }}
          component={DomainEditForm}
          onSubmit={onSubmit}
        />
      </DialogContent>
    </Dialog>
  )
}
