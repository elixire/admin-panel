import { Button, MenuItem } from '@blueprintjs/core'
import { Select } from '@blueprintjs/select'
import React from 'react'
import debounce from 'lodash.debounce'

import User from '../api/user'

export default class UserSuggestor extends React.Component {
  state = {
    users: [],
  }

  componentDidMount() {
    this.fetchUsers('')
  }

  itemRenderer = (user, { handleClick, modifiers }) => (
    <MenuItem
      active={modifiers.active}
      disabled={modifiers.disabled}
      key={user.name}
      icon="user"
      onClick={handleClick}
      text={user.name}
    />
  )

  itemPredicate = (query, user) =>
    user.name.toLowerCase().includes(query.toLowerCase())

  async fetchUsers(query) {
    const { results: users } = await User.search({
      active: true,
      page: 0,
      per_page: 10,
      query,
    })
    this.setState({ users })
  }

  render() {
    const { value = {} } = this.props

    return (
      <Select
        itemPredicate={this.itemPredicate}
        items={this.state.users}
        itemRenderer={this.itemRenderer}
        onItemSelect={this.handleItemSelect}
        onQueryChange={this.handleQueryChange}
        popoverProps={{
          minimal: true,
        }}
      >
        <Button text={value.name} icon="user" rightIcon="caret-down" />
      </Select>
    )
  }

  handleItemSelect = (item, event) => {
    // Prevent the selection from closing the parent popover, if any.
    event.stopPropagation()

    if (this.props.onChange) {
      this.props.onChange(item)
    }
  }

  handleQueryChange = debounce(async (query) => {
    await this.fetchUsers(query)
  }, 500)
}
