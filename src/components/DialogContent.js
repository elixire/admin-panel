import styled from 'styled-components'

const DialogBody = styled.div`
  padding: 1rem 1rem 0 1rem;
`

export default DialogBody
