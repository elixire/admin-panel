import { Button, ButtonGroup } from '@blueprintjs/core'
import React from 'react'
import 'styled-components/macro'

export default function PaginatorControl({ page, pagination, onPageChanged }) {
  const maxIndex = pagination.total - 1
  const onFirstPage = page === 0
  const onLastPage = page === maxIndex
  const noItems = pagination.total === 0

  function handleBack() {
    onPageChanged(Math.max(0, page - 1))
  }

  function handleForward() {
    onPageChanged(Math.min(maxIndex, page + 1))
  }

  function handleFirst() {
    onPageChanged(0)
  }

  function handleLast() {
    onPageChanged(maxIndex)
  }

  return (
    <ButtonGroup css="margin: 1rem 0;">
      {/* back button */}
      <Button icon="arrow-left" disabled={onFirstPage} onClick={handleBack} />

      {/* the first page */}
      {!onFirstPage ? <Button onClick={handleFirst}>1</Button> : null}

      {/* the current page */}
      <Button disabled>{page + 1}</Button>

      {/* the last page */}
      {!onLastPage && !noItems ? (
        <Button onClick={handleLast}>{pagination.total}</Button>
      ) : null}

      {/* forward button */}
      <Button
        icon="arrow-right"
        disabled={onLastPage || noItems}
        onClick={handleForward}
      />
    </ButtonGroup>
  )
}
