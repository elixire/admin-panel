import formDialog from '../formDialog'
import UserEditForm from '../forms/UserEditForm'

export default formDialog(UserEditForm, (props) => ({
  dialogProps: {
    icon: 'edit',
    title: `Edit ${props.user.name}`,
  },
  initialValues: {
    email: props.user.email,
    shortenLimit: props.user.limits.shortenLimit,
    uploadLimit: props.user.limits.uploadLimit,
  },
}))
