import React from 'react'
import { Link } from 'react-router-dom'

import ItemCard from './ItemCard'

export default function Shorten({ shorten, url }) {
  const { redirto: redir, deleted } = shorten
  const how = deleted ? 'Originally shortened' : 'Shortened'
  const uploader = shorten.uploader

  return (
    <ItemCard
      deleted={deleted}
      url={redir}
      title={`${url} → ${redir}`}
      item={shorten}
    >
      {how} by <Link to={`/user/${uploader.id}`}>{uploader.name}</Link>
    </ItemCard>
  )
}
