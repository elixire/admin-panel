import React from 'react'
import { Form } from 'formik'
import { Button, FormGroup, TextArea, Intent } from '@blueprintjs/core'

import InputGroupField from './fields/InputGroupField'
import withField from './fields/withField'
import BroadcastAlert from '../components/BroadcastAlert'

const TextAreaField = withField(TextArea)

export default function BroadcastForm({ values, isSubmitting, submitForm }) {
  const [alertIsOpen, setAlertIsOpen] = React.useState(false)

  function handleAlertConfirm() {
    setAlertIsOpen(false)
    submitForm()
  }

  function handleAlertCancel() {
    setAlertIsOpen(false)
  }

  function handleSubmit(event) {
    event.preventDefault()
    setAlertIsOpen(true)
  }

  return (
    <Form onSubmit={handleSubmit}>
      <BroadcastAlert
        isOpen={alertIsOpen}
        onConfirm={handleAlertConfirm}
        onCancel={handleAlertCancel}
      />

      <FormGroup label="Subject">
        <InputGroupField
          name="subject"
          placeholder="Service announcement"
          required
        />
      </FormGroup>

      <FormGroup label="Body">
        <TextAreaField
          name="body"
          placeholder="..."
          fill
          style={{ minHeight: '10rem' }}
        />
      </FormGroup>

      <Button
        type="submit"
        loading={isSubmitting}
        intent={Intent.SUCCESS}
        icon="headset"
      >
        Broadcast
      </Button>
    </Form>
  )
}
