import React from 'react'
import { Button, FormGroup, Intent } from '@blueprintjs/core'
import { Form } from 'formik'

import SwitchField from './fields/SwitchField'
import UserSuggestor from '../components/UserSuggestor'

export default function DomainEditForm({
  values,
  setFieldValue,
  isSubmitting,
}) {
  return (
    <Form>
      <FormGroup label="Options">
        <SwitchField name="adminOnly" label="Admin only" />
        <SwitchField name="official" label="Official" />
      </FormGroup>
      <FormGroup label="Domain owner">
        <UserSuggestor
          value={values.owner}
          onChange={(owner) => setFieldValue('owner', owner)}
        />
      </FormGroup>
      <Button
        type="submit"
        intent={Intent.SUCCESS}
        loading={isSubmitting}
        icon="edit"
      >
        Edit
      </Button>
    </Form>
  )
}
