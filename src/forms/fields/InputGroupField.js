import { InputGroup } from '@blueprintjs/core'
import withField from './withField'

export default withField(InputGroup)
