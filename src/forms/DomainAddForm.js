import React from 'react'
import { Button, FormGroup, Intent } from '@blueprintjs/core'
import { Form } from 'formik'

import SwitchField from './fields/SwitchField'
import InputGroupField from './fields/InputGroupField'

export default function DomainAddForm({ values, setFieldValue, isSubmitting }) {
  return (
    <Form>
      <FormGroup label="Domain" labelFor="domain-input" labelInfo="(required)">
        <InputGroupField name="domain" placeholder="example.com" required />
      </FormGroup>

      <FormGroup label="Options">
        <SwitchField name="adminOnly" label="Admin only" />
        <SwitchField name="official" label="Official" />
      </FormGroup>

      <Button
        type="submit"
        intent={Intent.SUCCESS}
        icon="plus"
        loading={isSubmitting}
      >
        Add
      </Button>
    </Form>
  )
}
