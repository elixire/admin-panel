import React from 'react'
import { Form } from 'formik'
import { FormGroup, Button, Intent } from '@blueprintjs/core'

import InputGroupField from './fields/InputGroupField'
import NumericInput from './inputs/NumericInput'

export default function UserEditForm({
  handleBlur,
  isSubmitting,
  setFieldValue,
  values,
}) {
  return (
    <Form>
      <FormGroup label="Email">
        <InputGroupField name="email" type="email" />
      </FormGroup>
      <FormGroup label="Upload limit" labelInfo="(bytes)">
        <NumericInput
          name="uploadLimit"
          onChange={setFieldValue}
          onBlur={handleBlur}
          value={values.uploadLimit}
          placeholder="100000000"
        />
      </FormGroup>
      <FormGroup label="Shorten limit" labelInfo="(number of shortens)">
        <NumericInput
          name="shortenLimit"
          onChange={setFieldValue}
          onBlur={handleBlur}
          value={values.shortenLimit}
          placeholder="0"
        />
      </FormGroup>
      <Button
        loading={isSubmitting}
        intent={Intent.SUCCESS}
        type="submit"
        icon="tick"
      >
        Save
      </Button>
    </Form>
  )
}
