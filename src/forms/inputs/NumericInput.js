import React from 'react'
import { NumericInput as BlueprintNumericInput } from '@blueprintjs/core'

export default function NumericInput({
  name,
  onChange,
  value,
  onBlur,
  ...props
}) {
  return (
    <BlueprintNumericInput
      onValueChange={(value) => onChange(name, value)}
      value={value}
      {...props}
    />
  )
}
