/* global BigInt */

const EPOCH = 1420070400000

export function toDate(snowflake) {
  if (typeof BigInt !== 'undefined') {
    const epoch = BigInt(EPOCH)
    const stamp = epoch + (BigInt(snowflake) >> BigInt(22))
    return new Date(Number(stamp))
  }

  // Tiemen#0001 (152164749868662784) made this.
  return new Date(parseInt(snowflake, 10) / 4194304 + EPOCH)
}
