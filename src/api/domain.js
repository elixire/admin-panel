import request from '.'
import User from './user'
import { query } from '../util'

/**
 * Expands an object of ID to Domain to an array of inflated Domain objects.
 */
function expandDomains(info) {
  return Object.entries(info).map(
    ([id, info]) => new Domain(parseInt(id, 10), info)
  )
}

export default class Domain {
  static async fetchById(id) {
    const info = await request(`/api/admin/domains/${id}`)
    return new Domain(id, info)
  }

  /**
   * Fetch all domains that the current user owns.
   */
  static async fetchMine() {
    const info = await request('/api/stats/my_domains')
    return expandDomains(info)
  }

  static async search(params) {
    const resp = await request(`/api/admin/domains/search` + query(params))
    const { pagination, results } = resp
    return { pagination, results: expandDomains(results) }
  }

  /**
   * Fetch all domains with additional information that only admins can access.
   *
   * As of 2018.2.10, in order to prevent breaking changes, this endpoint will
   * NOT paginate unless you provide a page parameter. Otherwise, solely the
   * array is returned.
   */
  static async fetchAllAdmin(params = {}) {
    const route = '/api/admin/domains' + query(params)
    const resp = await request(route)

    if ('pagination' in resp) {
      const { pagination, ...domains } = resp
      return { pagination, results: expandDomains(domains) }
    } else {
      return { results: expandDomains(resp) }
    }
  }

  /**
   * Fetch all domains.
   */
  static async fetchAll(options) {
    const info = await request('/api/domains')

    // Official domain IDs are stored in a separate object, referenced by foreign
    // keys.
    return Object.entries(info.domains).map(([id, domain]) => ({
      domain,
      id: parseInt(id, 10),
      official: info.officialdomains.includes(parseInt(id, 10)),
    }))
  }

  static async create({ domain, adminOnly, official }) {
    await request('/api/admin/domains', {
      body: JSON.stringify({
        admin_only: adminOnly,
        domain,
        official,
      }),
      method: 'PUT',
    })
  }

  constructor(id, response) {
    const {
      info: {
        domain,
        official,
        admin_only: adminOnly,
        cf_enabled: cfEnabled,
        owner,
      },
      stats,
      public_stats: publicStats,
    } = response

    Object.assign(this, {
      adminOnly,
      cfEnabled,
      domain,
      id,
      official,
      owner: owner == null ? null : new User(owner),
      publicStats,
      stats,
    })
  }

  get validDomain() {
    return this.domain.replace('*', 'wildcard')
  }

  toString() {
    return this.domain
  }

  async delete() {
    await request(`/api/admin/domains/${this.id}`, { method: 'DELETE' })
  }

  async edit({ owner, adminOnly, ...options }) {
    const route = `/api/admin/domains/${this.id}`

    if (owner && !this.owner) {
      await request(`${route}/owner`, {
        body: JSON.stringify({ owner_id: owner.id }),
        method: 'PUT',
      })
    }

    await request(route, {
      body: JSON.stringify({
        ...options,
        ...(adminOnly != null ? { admin_only: adminOnly } : {}),
        ...(owner != null && this.owner ? { owner_id: owner.id } : {}),
      }),
      method: 'PATCH',
    })
  }

  async setOwner(user) {
    if (!this.owner) {
      return
    }

    await request(`/api/admin/domains/${this.id}`, {
      body: JSON.stringify({
        owner_id: user.id,
      }),
      method: 'PATCH',
    })
  }
}
