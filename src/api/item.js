import Domain from './domain'
import User from './user'

/**
 * Represents a shorten or file on elixire.
 */
export default class Item {
  constructor({ filename: shortcode, domain, uploader, deleted }) {
    Object.assign(this, {
      deleted,
      domain,
      shortcode,
      uploader,
    })
  }

  /**
   * Fetches the uploader of this item.
   */
  async fetchUploader() {
    if (typeof this.uploader !== 'string') {
      return
    }

    const user = await User.fetchById(this.uploader)
    this.uploader = user
  }

  /**
   * Fetches the domain that this item is stored on.
   */
  async fetchDomain() {
    if (typeof this.domain !== 'number') {
      return
    }

    const domain = await Domain.fetchById(this.domain)
    this.domain = domain
  }
}
