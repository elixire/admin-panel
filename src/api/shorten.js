import Item from './item'
import request from './request'

export default class Shorten extends Item {
  static async getByShortcode(shortcode) {
    try {
      const info = await request(`/api/admin/shorten/${shortcode}`)
      return new Shorten(info)
    } catch (err) {
      if (err.resp.status === 404) {
        return null
      }
      throw err
    }
  }

  constructor(payload) {
    super(payload)

    this.id = payload.shorten_id
    this.redirto = payload.redirto
  }

  toString() {
    return this.shortcode
  }

  /**
   * Resolve the URL to this shorten. If this file has domain information, it is
   * used automatically. You should provide a fallback to be used in case we only
   * have the ID.
   * @param {string} domainName
   */
  getUrl(domainName) {
    const domain =
      typeof this.domain === 'number' ? domainName : this.domain.validDomain

    return `https://${domain}/s/${this.shortcode}`
  }

  async delete() {
    await request(`/api/admin/shorten/${this.id}`, { method: 'DELETE' })
  }

  async rename(to) {
    await request(`/api/admin/shorten/${this.id}`, {
      body: JSON.stringify({ shortname: to }),
      method: 'PATCH',
    })
  }

  async move(to) {
    await request(`/api/admin/shorten/${this.id}`, {
      body: JSON.stringify({ domain_id: to.id }),
      method: 'PATCH',
    })
  }
}
