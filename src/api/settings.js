import request from './request'

export default class Settings {
  static async get() {
    return request('/api/admin/settings')
  }

  static async set(settings) {
    return request('/api/admin/settings', {
      body: JSON.stringify(settings),
      method: 'PATCH',
    })
  }
}
