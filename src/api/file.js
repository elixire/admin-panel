import Item from './item'
import request from './request'

export default class File extends Item {
  static async getByShortcode(shortcode) {
    try {
      const info = await request(`/api/admin/file/${shortcode}`)
      return new File(info)
    } catch (err) {
      if (err.resp.status === 404) {
        return null
      }
      throw err
    }
  }

  constructor(payload) {
    super(payload)

    const { file_id: id, mimetype, fspath: path, file_size: size } = payload

    Object.assign(this, {
      id,
      mimetype,
      path,
      size,
    })
  }

  toString() {
    return `${this.shortcode}.${this.getExtension()}`
  }

  getExtension() {
    const parts = this.path.split('.')
    const extension = parts[parts.length - 1]
    return extension
  }

  /**
   * Resolve the URL to this file. If this file has domain information, it is
   * used automatically. You should provide a fallback to be used in case we only
   * have the ID.
   * @param {string} domainName
   */
  getUrl(domainName) {
    const domain =
      typeof this.domain === 'number' ? domainName : this.domain.validDomain

    return `https://${domain}/i/${this.shortcode}.${this.getExtension()}`
  }

  async delete() {
    await request(`/api/admin/file/${this.id}`, { method: 'DELETE' })
  }

  async rename(to) {
    await request(`/api/admin/file/${this.id}`, {
      body: JSON.stringify({ shortname: to }),
      method: 'PATCH',
    })
  }

  async move(to) {
    await request(`/api/admin/file/${this.id}`, {
      body: JSON.stringify({ domain_id: to.id }),
      method: 'PATCH',
    })
  }
}
