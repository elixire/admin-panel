import CONFIG from '../config.json'
import strictFetch from '../fetch'
import { sleep } from '../util'

const ratelimits = {
  backlogged: 0,
  remaining: null,
  reset: null,
}

export default async function request(route, params = {}) {
  const options = {
    headers: {
      Authorization: params.token || window.localStorage.getItem('token'),
    },
    ...params,
  }

  let timeLeft = ratelimits.reset * 1000 - Date.now()
  timeLeft = timeLeft < 0 ? null : timeLeft
  console.log(
    '%c[Rate Limit]%c Remaining requests: %s / Resetting in %s (backlogged: %d)',
    'color: purple; font-weight: bold;',
    'color: inherit; font-weight: inherit;',
    ratelimits.remaining || '[n/a]',
    timeLeft || '[n/a]',
    ratelimits.backlogged
  )

  if (ratelimits.remaining === 0) {
    // We have run out of requests.
    ratelimits.backlogged++

    // Calculate the time to wait in addition to the reset timestamp.
    // With every backlogged request (request made while waiting), we increase
    // the wait time exponentially so we don't bust the ratelimit immediately
    // after it resets.
    const additional = (1.3 ** ratelimits.backlogged - 1) * 1500
    timeLeft += additional

    console.warn(
      'Ran out of requests. Taking a %dms long nap. (+%dms)',
      timeLeft,
      additional
    )

    await sleep(timeLeft)
  }

  try {
    var resp = await strictFetch(CONFIG.instance + route, options)

    ratelimits.remaining = parseInt(resp.headers.get('x-ratelimit-remaining'))
    ratelimits.reset = parseFloat(resp.headers.get('x-ratelimit-reset'), 10)

    if (ratelimits.remaining > 0) {
      // Reset backlogged request counter -- we are no longer being ratelimited.
      ratelimits.backlogged = 0
    }
  } catch (err) {
    if (err.resp && err.resp.status === 429) {
      console.warn('Received HTTP 429 from server.')

      // We are being ratelimited now.
      ratelimits.remaining = 0
      ratelimits.reset = Date.now() / 1000 + err.body.retry_after

      return request(route, params)
    } else {
      throw err
    }
  }

  if (resp.status === 204) {
    return
  }

  const body = await resp.json()
  return body
}
