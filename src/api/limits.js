export class Limits {
  constructor({
    used: uploadUsed,
    limit: uploadLimit,
    shortenused: shortensUsed,
    shortenlimit: shortenLimit,
  }) {
    Object.assign(this, {
      shortenLimit,
      shortensUsed,
      uploadLimit,
      uploadUsed,
    })
  }

  get uploadPercentage() {
    return this.uploadUsed / this.uploadLimit
  }

  get shortensPercentage() {
    return this.shortensUsed / this.shortenLimit
  }
}
