import '@blueprintjs/core/lib/css/blueprint.css'
import '@blueprintjs/icons/lib/css/blueprint-icons.css'
import 'normalize.css/normalize.css'

import './index.css'

import { FocusStyleManager } from '@blueprintjs/core'

import React from 'react'
import ReactDOM from 'react-dom'

import Root from './components/Root'

FocusStyleManager.onlyShowFocusOnTabs()

const mount = document.getElementById('app-mount')
ReactDOM.render(<Root />, mount)
