const NUMBER_FORMATTER =
  typeof Intl === 'object' ? new Intl.NumberFormat() : null

export function extractId(item) {
  if (item.file_id) {
    return item.file_id
  } else {
    return item.shorten_id
  }
}

export function formatNumber(number) {
  return NUMBER_FORMATTER == null
    ? String(number)
    : NUMBER_FORMATTER.format(number)
}

export function pluralize(name, value) {
  const prettyNumber = formatNumber(value)
  return value === 1 ? `${prettyNumber} ${name}` : `${prettyNumber} ${name}s`
}

export function pluralizeValues(map) {
  return Object.entries(map).map(([item, value]) =>
    pluralize(item.replace(/s$/, ''), value)
  )
}

export function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms)
  })
}

export function transformKeys(object, keys) {
  return Object.entries(object).reduce((acc, [key, value]) => {
    return { ...acc, [keys[key] || key]: value }
  }, {})
}

export function query(params) {
  return (
    '?' +
    Object.entries(params)
      .map(([k, v]) => `${k}=${encodeURIComponent(v)}`)
      .join('&')
  )
}
