# elixire-admin-panel

An admin panel for elixire written in JavaScript using React and
styled-components.

## Configuring

Copy the example configuration:

```sh
$ cp config.example.json config.json
```

Schema:

```js
{
  // The API endpoint that the admin panel should use. Don't include a trailing
  // slash.
  endpoint: string,

  // The path basename that the router will use.
  basename: string,
}
```

If you change `basename`, you will have to also change `homepage` in
`package.json`.

By default, the basename is `/admin`.

## Building

```sh
$ npm install
$ npm run build
```

Static files will be in `build/`.
